## Install application
- copy .env.example to .env file, add db connection settings, add `APP_URL` param for some local host to make download links work properly for this app, leave `QUEUE_CONNECTION` as is for queues working on db
- basic install: composer install, key generate
- run commands
```bash
php artisan migrate
php artisan queue:work
```

## REST API
- POST `/api/download` with JSON payload, e.g.:
```JSON
{
  "urls": [
    "https://www.google.com/search?sxsrf=ACYBGNT6LJq3ph6SF9XdA7INFLmsUTF-5A:1576824339333&q=images&tbm=isch&source=univ&sa=X&ved=2ahUKEwjtzvm00MPmAhULE8AKHcxlBTsQsAR6BAgKEAE&biw=1920&bih=969#imgrc=hVqSE9l6BNjL6M&imgdii=LbrpLW13XF_0vM",
    "http://profiptm.com/profobrabotka_foto_profiptm/",
    "http://profiptm.com/wp-content/uploads/2014/05/10-300x169.png"
  ]
}
```
*Response:*
```JSON
{
  "status": "ok",
  "data":[]
}
```
You can use any quantity for urls array. Urls go to the queue and will be processed by worker

- GET `/api/list?page=` with optional GET param `page` (20 items per page)

*Response:*
```JSON
{
  "status": "ok",
  "data": {
    "tasks": {
      "current_page": 1,
      "data": [
        {"id": 6, "status": "complete", "source_url": "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQgUyjDRLUJZt6gH2I5OlZwXuyFm395m1qwKfwbYkeEp88KvLl_",…},
        {"id": 7, "status": "complete", "source_url": "https://www.google.com/search?q=example&sxsrf=ACYBGNQgefiGb0JLqg3gVx2TyUvuxZbwxg:1576752146243&tbm=isch&source=iu&ictx=1&fir=LY8zIFojzlyL0M%253A%252ChCNwoIMhLkhY6M%252C%252Fm%252F03f5n1_&vet=1&usg=AI4_-kRTXTQyya3sEIGWC7ULn50Ldrejig&sa=X&ved=2ahUKEwiTis28w8HmAhUI_CoKHUoUD8IQ_B0wG3oECAkQAw#imgrc=LY8zIFojzlyL0M&imgdii=GhQVKTkAv5eIqM",…}
      ],
      "first_page_url": "http://url-download.loc/api/list?page=1",
      "from": 1,
      "last_page": 4,
      "last_page_url": "http://url-download.loc/api/list?page=4",
      "next_page_url": "http://url-download.loc/api/list?page=2",
      "path": "http://url-download.loc/api/list",
      "per_page": 2,
      "prev_page_url": null,
      "to": 2,
      "total": 8
    }
  }
}
```

## HTML
There is a HTML analogue for `list` method: GET `/list?page=`. Here you can see items with download links

## Tests
To run tests (on clean db, I don't clear db automatically):
```bash
php vendor/phpunit/phpunit/phpunit tests/Feature/Api
```
