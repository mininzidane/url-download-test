<?php
declare(strict_types=1);

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Responses\SuccessJsonResponse;
use App\Jobs\QueueTask;
use App\Models\Task;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class DownloadController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $data = $request->getContent();
        $data = \GuzzleHttp\json_decode($data, true);
        $urls = $data['urls'] ?? [];

        foreach ($urls as $url) {
            $task = Task::create($url);
            QueueTask::dispatch($task);
        }
        return new SuccessJsonResponse();
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $tasks = Task::paginate(self::PAGE_SIZE);

        return new SuccessJsonResponse([
            'tasks' => $tasks
        ]);
    }
}
