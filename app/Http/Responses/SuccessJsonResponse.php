<?php

namespace App\Http\Responses;

use Illuminate\Http\JsonResponse;

class SuccessJsonResponse extends JsonResponse
{
    /**
     * SuccessJsonResponse constructor.
     * @param array $data
     * @param int $status
     * @param array $headers
     * @param int $options
     */
    public function __construct(array $data = [], int $status = 200, array $headers = [], int $options = 0)
    {
        parent::__construct(['status' => 'ok', 'data' => $data], $status, $headers, $options);
    }
}
