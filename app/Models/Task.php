<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Task
 *
 * @package App\Models
 * @property int $id
 * @property string $status
 * @property string $source_url
 * @property string $url
 * @property string $created_at
 * @property string $updated_at
 * @property-write mixed $raw
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereSourceUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereUrl($value)
 * @mixin \Eloquent
 */
class Task extends Model
{
    public const STATUS_PENDING = 'pending';
    public const STATUS_DOWNLOADING = 'downloading';
    public const STATUS_COMPLETE = 'complete';
    public const STATUS_ERROR = 'error';

    /**
     * @param string $url
     * @return static|null
     */
    public static function create(string $url): ?self
    {
        $task = new self;
        $task->status = self::STATUS_PENDING;
        $task->source_url = $url;
        if (!$task->save()) {
            logger()->error('Error on creating task', ['Task' => $task]);
            return null;
        }
        return $task;
    }

    /**
     * @param string $status
     * @return bool
     */
    public function setStatus(string $status): bool
    {
        $this->status = $status;
        return $this->save();
    }

    /**
     * @param string|null $url
     * @return string|null
     */
    public function getUrlAttribute(?string $url): ?string
    {
        if ($url === null) {
            return null;
        }

        $storage = \Storage::disk('downloadStorage');
        return $storage->url($url);
    }
}
