<?php

namespace App\Services;

use App\Models\Task;
use GuzzleHttp\Client;

class DownloadService
{
    /**
     * @param Task $task
     * @return bool
     */
    public function downloadContent(Task $task): bool
    {
        $task->setStatus(Task::STATUS_DOWNLOADING);

        try {
            $client = new Client();
            $response = $client->request('GET', $task->source_url);
            $html = (string)$response->getBody();

        } catch (\Throwable $e) {
            $task->setStatus(Task::STATUS_ERROR);
            return false;
        }
        $driver = \Storage::disk('downloadStorage');
        $filename = \md5($task->source_url);

        $success = $driver->put($filename, $html);
        if (!$success) {
            $task->setStatus(Task::STATUS_ERROR);
            logger()->error('Error saving resource', ['filename' => $filename]);
        }

        $task->url = $filename;
        return $task->setStatus(Task::STATUS_COMPLETE);
    }
}
