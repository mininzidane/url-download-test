<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .error {
            background: red;
        }
    </style>
</head>
<body>
<div class="container" style="padding: 0 50px">
    <div class="flex-center position-ref full-height">
        <table class="table">
            <tr>
                <th>ID</th>
                <th>Source</th>
                <th></th>
                <th>Created date</th>
            </tr>
            @foreach ($tasks as $task)
                <tr class="{{ $task->status === \App\Models\Task::STATUS_ERROR ? 'error' : '' }}">
                    <td>{{ $task->id }}</td>
                    <td>
                        <a href="{{ $task->source_url }}" target="_blank">{{ \substr($task->source_url, 0, 50) }}</a>
                    </td>
                    <td>
                        @if ($task->status === \App\Models\Task::STATUS_COMPLETE)
                            <a href="{{ $task->url }}" target="_blank">download</a>
                        @endif
                    </td>
                    <td>{{ $task->created_at->format('Y-m-d') }}</td>
                </tr>
            @endforeach
        </table>

        {{ $tasks->render() }}
    </div>
</div>
</body>
</html>
