<?php
declare(strict_types=1);

namespace Tests\Feature\Api;

use App\Models\Task;
use Tests\TestCase;

class DownloadControllerTest extends TestCase
{
    /**
     * @dataProvider urlsProvider
     * @param array $urls
     * @param array $statuses
     * @throws \Exception
     */
    public function testCreateAndList(array $urls, array $statuses): void
    {
        // check API create task
        $response = $this->json('POST', '/api/download', [
            'urls' => $urls,
        ]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => 'ok',
                'data' => [],
            ])
        ;

        // check API list for 3d page (empty)
        $response = $this->json('GET', '/api/list?page=3');
        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => 'ok',
            ])
        ;
        $data = $response->json('data');
        $this->assertEmpty($data['tasks']['data']);

        // check API list for 1st page
        $response = $this->json('GET', '/api/list');
        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => 'ok',
            ])
        ;
        $data = $response->json('data');
        $tasks = $data['tasks']['data'];
        $this->assertCount(\count($urls), $tasks);

        foreach ($urls as $i => $url) {
            $task = Task::whereSourceUrl($url)->first();
            // check status correct
            $this->assertEquals($statuses[$i], $task->status);

            // check list items through db items
            $taskRow = $tasks[$i];
            $this->assertEquals($task->id, $taskRow['id']);
            $this->assertEquals($task->status, $taskRow['status']);
            $this->assertEquals($task->source_url, $taskRow['source_url']);
            $this->assertEquals($task->url, $taskRow['url']);

            // check resource saved
            if ($statuses[$i] === Task::STATUS_COMPLETE) {
                $storage = \Storage::disk('downloadStorage');
                $parts = \explode('/', $task->url);
                $filename = \end($parts);
                $this->assertTrue($storage->exists($filename));
            }

            $task->delete();
        }
    }

    /**
     * @return array
     */
    public function urlsProvider(): array
    {
        return [
            [
                [
                    'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQgUyjDRLUJZt6gH2I5OlZwXuyFm395m1qwKfwbYkeEp88KvLl_',
                    'https://www.google.com/search?q=example&sxsrf=ACYBGNQgefiGb0JLqg3gVx2TyUvuxZbwxg:1576752146243&tbm=isch&source=iu&ictx=1&fir=LY8zIFojzlyL0M%253A%252ChCNwoIMhLkhY6M%252C%252Fm%252F03f5n1_&vet=1&usg=AI4_-kRTXTQyya3sEIGWC7ULn50Ldrejig&sa=X&ved=2ahUKEwiTis28w8HmAhUI_CoKHUoUD8IQ_B0wG3oECAkQAw#imgrc=LY8zIFojzlyL0M&imgdii=GhQVKTkAv5eIqM',
                    'http://google.com',
                    'http://jklksjkdfjlfdlskjf',
                    '333434343'
                ],
                [
                    Task::STATUS_COMPLETE,
                    Task::STATUS_COMPLETE,
                    Task::STATUS_COMPLETE,
                    Task::STATUS_ERROR,
                    Task::STATUS_ERROR,
                ]
            ]
        ];
    }
}
